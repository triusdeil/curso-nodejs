/*
const http = require('http');
const color = require('colors'); 
const HandleServer = function(req/*request == pregunta*//*,res*//*response = respuesta ){
    res.writeHead(200, {'Content-type':'text/html'});
    res.write('<h1>Hola Mundo </h1>');
    res.end();
}
const server = http.createServer(HandleServer);

server.listen(3000,function(){
    console.log('servidor en puerto 3000'.magenta);
})
*/
const express = require('express');
const colors = require('colors');

const server = express();

server.get('/', function(req,res){
    res.send('<h1>Hola Mundo</h1>');
    res.end();
});

server.listen(3000, function(){
    console.log("Estamos utilizando el servidor 3000".magenta);
})